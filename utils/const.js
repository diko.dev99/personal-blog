export const materi = [
  {
    id: 1,
    title: 'ReactJs',
    description: 'Belajar vuejs dari 0 sampai dengan tahap deployment aplikasi',
    img: 'reactjs.png'
  },
  {
    id: 2,
    title: 'VueJs',
    description: 'Belajar vuejs dari 0 sampai dengan tahap deployment aplikasi',
    img: 'vuejs.png'
  },
  {
    id: 3,
    title: 'PHP Laravel',
    description: 'Belajar vuejs dari 0 sampai dengan tahap deployment aplikasi',
    img: 'laravel.png'
  },
  {
    id: 4,
    title: 'NodeJs',
    description: 'Belajar vuejs dari 0 sampai dengan tahap deployment aplikasi',
    img: 'node.png'
  }
]

export const pullers = [
  {
    id: 1,
    title: 'Studi Kasus',
    description: 'Up-to-date',
    img: 'easy.png'
  },
  {
    id: 2,
    title: 'Modules',
    description: 'Dasar-Expert',
    img: 'Binocular.png'
  },
  {
    id: 3,
    title: 'Konsultasi',
    description: 'Seumur Hidup',
    img: 'mentor.png'
  },
  {
    id: 4,
    title: 'Sertifikat',
    description: 'Kelulusan Kelas',
    img: 'certicate.png'
  },
  {
    id: 5,
    title: 'Mentor',
    description: 'Profesional',
    img: 'Teamwork.png'
  },
  {
    id: 6,
    title: 'Bahasa',
    description: 'Bahasa Indonesia',
    img: 'idea.png'
  },
  {
    id: 7,
    title: 'Akses',
    description: 'Dimana saja',
    img: 'cloud.png'
  },
  {
    id: 8,
    title: 'Teman',
    description: '1k+ Pengguna',
    img: 'relation.png'
  },
  {
    id: 9,
    title: 'Dicarikan Kerja',
    description: 'Ketika sudah lulus',
    img: 'companies.png'
  }
]

export const generations = [
  {
    id: 1,
    title: 'Memulai',
    description: 'Ada banyak kelas menarik sesuai dengan minatmu',
    img: '1.png'
  },
  {
    id: 2,
    title: 'Mempelajari',
    description: 'Belajar langusung dari video tutorial berkualitas tinggi',
    img: '2.png'
  },
  {
    id: 3,
    title: 'Sharing',
    description: 'Problem solving masalahmu dengan engineer lainnya',
    img: '3.png'
  }
]
