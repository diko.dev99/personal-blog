import { materi, pullers, generations } from '~/utils/const'

export const getters = {
  materi () {
    return materi
  },
  pullers () {
    return pullers
  },
  generations () {
    return generations
  }
}
