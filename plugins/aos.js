import Vue from 'vue'

import AOS from 'aos'

Vue.use(AOS.init())
window.addEventListener('load', AOS.refresh)
